﻿using System;
using System.Linq;

namespace Analyzer
{
    class Program 
    {
        static void Main(string[] args)
        {
            if (args.Count() == 0) NotValid();
            Console.WriteLine("Walcome To Analyzer " + System.Reflection.Assembly.GetEntryAssembly().GetName().Version);

            string option = args[0];
            switch (option.ToLower())
            {
                case "vt":
                    Processes.scanOnVT();
                    break;
                case "word":
                    if (args.Count() < 3) NotValid();

                    Console.WriteLine("Starting Search For Files");

                    Int32 found = args[1].ToLower() == "all" ? FileSearcher.SearchDrives() : FileSearcher.SearchFolders(args[1]);

                    Console.WriteLine(String.Format("\nFound {0} Documents.. Starting Analysis\n", found));
                    FileSearcher.FindWords(args[2]);
                    break;
                case "help":
                    Console.WriteLine(@"
                    Analyzer currently support 2 options:
                    VT - Will run all processes in Virus Total
                    Word - Will check all files with spechific extension and look for word from word list
                        Extenstions: docx, pdf
                    ");
                    break;
                default:
                    Console.WriteLine("Unknown Action Entered\nHave support in: VT");
                    break;
            }
        }

        static void NotValid()
        {
            Console.WriteLine(
                @"Not Valid, Options:
                    Analyzer.exe vt
                    Analyzer.exe word d:\ d:\wordlist.txt
                    Analyzer.exe word all d:\wordlist.txt
                ");
            Environment.Exit(3);
        }
    }

    

    

    



}
