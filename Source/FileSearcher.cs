﻿using Novacode;
using PdfToText;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace Analyzer
{
    static class FileSearcher
    {
        internal static List<String> SearchResult = new List<string>();
        internal static String[] WordList;
        private static Int32 Counter = 0;
        internal static int SearchDrives()
        {
            foreach (String drive in Directory.GetLogicalDrives())
            {
                try
                {
                    SearchFolders(drive);
                }
                catch (Exception) { }
            }

            return SearchResult.Count();
        }

        internal static int SearchFolders(String prmPath)
        {
            try
            {
                foreach (String folder in Directory.GetDirectories(prmPath))
                {
                    Counter++;
                    Counter.ProgressBar(50);
                    if (folder.ToLower().Contains("\\windows") || folder.ToLower().Contains("\\program files"))
                        continue;

                    SearchFolders(folder);
                    SearchFiles(folder);
                }
            }
            catch (Exception) { }

            return SearchResult.Count();
        }

        internal static int SearchFiles(String prmPath)
        {
            try
            {
                foreach (String file in Directory.GetFiles(prmPath))
                {
                    FileInfo info = new FileInfo(file);
                    if (Settings.FileType.Contains(info.Extension.ToLower()))
                    {
                        SearchResult.Add(info.FullName);
                    }
                }
            }
            catch (Exception) { }

            return SearchResult.Count();
        }

        internal static void FindWords(string wordsFileName)
        {
            if (SearchResult.Count() == 0)
                return;

            WordList = System.IO.File.ReadAllLines(wordsFileName, Encoding.Default);
            if (WordList.Count() == 0)
                return;

            Int32 keepAlive = 1;
            string tempFile = Path.GetTempPath() + "\\pdftemp.txt";

            foreach (string file in SearchResult)
            {
                string ext = Path.GetExtension(file);
                string word = "";
                switch (ext)
                {
                    case ".docx":
                        try
                        {
                            using (DocX document = DocX.Load(file))
                            {

                                Boolean Result = lookForWords(document.Text, out word);
                                if (Result)
                                    Console.WriteLine(
                                        String.Format("\n*** Found File With The Word = '{0}' ***\n{1}", word, file));
                                else
                                    keepAlive++;
                            }
                        }
                        catch (FileFormatException)
                        {
                            Console.WriteLine("Failed Reading: " + file + "\ncontains corrupted data");
                        }
                        catch (Exception e)
                        {
                            Console.WriteLine("Failed Reading: " + file);
                            Console.WriteLine(e.ToString());
                        }
                        break;
                    case ".pdf":
                        try
                        {
                            PDFParser pdfParser = new PDFParser();
                            pdfParser.ExtractText(file, tempFile);
                            string result = File.ReadAllText(tempFile);

                            Boolean Result = lookForWords(result, out word);
                            if (Result)
                                Console.WriteLine(
                                    String.Format("\n*** Found File With The Word = '{0}' ***\n{1}", word, file));
                            else
                                keepAlive++;


                        }
                        catch (Exception e)
                        {

                        }
                        break;
                }
                keepAlive.ProgressBar(10);
            }
            try { File.Delete(tempFile); }
            catch (Exception) { }
        }

        private static bool lookForWords(string documentText, out string chosenWord)
        {
            documentText = documentText.ToLower();
            foreach (string word in WordList)
            {
                if (documentText.IndexOf(word) != -1)
                {
                    chosenWord = word;
                    return true;
                }
            }
            chosenWord = "";
            return false;
        }
    }
}
