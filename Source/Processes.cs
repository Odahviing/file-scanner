﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Net;

namespace Analyzer
{
    internal static class Processes
    {
        private static Process[] processList;
        private static Process[] updateProcesssList()
        {
            return Process.GetProcesses();
        }
        private static Dictionary<String, String> extractProcessFileName()
        {
            processList = updateProcesssList();
            Dictionary<String, String> exeFiles = new Dictionary<string, string>();

            foreach (var process in processList)
            {
                try
                {
                    if (!exeFiles.Keys.Contains(process.ProcessName))
                        exeFiles.Add(process.ProcessName, process.MainModule.FileName);

                }
                catch (Win32Exception)
                {
                    Console.WriteLine("Can't Touch - " + process.ProcessName);
                }
            }

            return exeFiles;
        }

        internal static void scanOnVT()
        {
            Console.WriteLine("Starting Virus Total Scan");
            var fileNames = extractProcessFileName();
            Console.WriteLine(String.Format("Found {0} Process To Scan", fileNames.Count()));

            String UrlPathFormat = "https://www.virustotal.com/en/file/{0}/analysis/";
            WebClient client = new WebClient();
            client.Headers[HttpRequestHeader.UserAgent] = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/535.2 (KHTML, like Gecko) Chrome/15.0.874.121 Safari/535.2";
            client.Headers.Add("accept-language", "en-US,en;q=0.8");

            foreach (var file in fileNames)
            {
                string _debug = "";
                try
                {
                    string UrlPath = String.Format(UrlPathFormat, Helper.getSha256(file.Value));
                    var stringData = (client.DownloadString(UrlPath));
                    if (stringData.IndexOf("File not found") != -1)
                    {
                        Console.WriteLine("Unknown Process: " + file + "] \nFile: " + file.Value);
                        continue;
                    }
                    _debug = stringData;
                    var index = stringData.IndexOf("detected the file as malicious");
                    _debug = stringData.Substring(index - 24, 12);
                    var results = stringData.Substring(index - 24, 12).Split(' ');

                    if (int.Parse(results[0]) == 0)
                        Console.WriteLine(file.Key + " Clean");
                    else
                        Console.WriteLine("[Process: " + file + "] Not Clean\n File: " + file.Value + "\n Result:" + results[0] + " / " + results[3]);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.ToString());
                }
            }
        }
    }
}
