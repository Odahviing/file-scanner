﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;

namespace Analyzer
{
    internal static class Helper
    {
        internal static String getSha256(string fileName)
        {
            try
            {
                using (FileStream stream = File.OpenRead(fileName))
                {
                    SHA256Managed sha = new SHA256Managed();
                    byte[] hash = sha.ComputeHash(stream);
                    return BitConverter.ToString(hash).Replace("-", String.Empty);
                }
            }
            catch (UnauthorizedAccessException)
            {
                Console.WriteLine("Can't Calc Hash To: " + fileName);
                return "";
            }
        }
        internal static void ProgressBar(this Int32 key, int value)
        {
            if (key % value == 0) Console.Write(".");
        }
    }

    internal static class Settings
    {
        internal static List<String> FileType = new List<String>
        {
            ".docx",
            ".pdf",
            
        };

        /* Need To Add
        ".doc",
        ".xlsx",
        ".xls",
        ".pptx",
        ".ppt"
        */
    }
}
